package query;

import java.util.ArrayList;
import region.Region;
import triple.ItemList;

public class SubregionSearcher {
    
    protected ItemList<Region> subregionList;
    protected ItemList<Integer> subregionIndexList;

    public SubregionSearcher(ItemList<Region> subregionList, ItemList<Integer> subregionIndexList) {
        this.subregionList = subregionList;
        this.subregionIndexList = subregionIndexList;
    }

    public ItemList<Integer> execute(Dimension dimension, short value) {
        final ItemList<Integer> pageIndex = new ItemList<>();
        for (int i = 0; i<subregionIndexList.size();i++){
            pageIndex.addAll(subregionList.get(subregionIndexList.get(i)).search(dimension, value));
        }
        return pageIndex;
    }
    
}
