package query;

public class Dimension {
    
    public static final Dimension X = new Dimension(0);
    public static final Dimension Y = new Dimension(1);
    
    private final int id;
    private Dimension orthogonal;

    private Dimension(int id) {
        this.id = id;
    }

    public Dimension getOrthogonal() {
        return orthogonal;
    }

    public int getId() {
        return id;
    }

    static {
        X.orthogonal = Y;
        Y.orthogonal = X;
    }
    
}
