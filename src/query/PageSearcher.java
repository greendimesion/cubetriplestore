package query;

import java.util.ArrayList;
import page.Page;
import triple.ItemList;

public class PageSearcher {

    protected ItemList<Page> pageList;
    protected ItemList<Integer> pageIndex;

    public PageSearcher(ItemList<Page> pageList, ItemList<Integer> pageIndex) {
        this.pageList = pageList;
        this.pageIndex = pageIndex;
    }

    public ArrayList<Long> execute(Dimension dimension, byte value) {
        final ArrayList<Long> result = new ArrayList<>();
        for (int i = 0; i < pageIndex.size(); i++) {
            result.addAll(pageList.get(pageIndex.get(i)).search(dimension, (byte) value));
        }
        return result;
    }

}
