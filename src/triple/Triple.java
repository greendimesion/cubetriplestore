package triple;

import java.io.Serializable;

public class Triple implements Serializable{
    
    private long subject;
    private int predicate;
    private long object;

    public Triple(long subject, int predicate, long object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public Triple() {
         
    }

    public long getSubject() {
        return subject;
    }

    public int getPredicate() {
        return predicate;
    }

    public long getObject() {
        return object;
    }
    
    public Triple buildTriple(long subject, int predicate, long object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
        return this;
    }
    
}
