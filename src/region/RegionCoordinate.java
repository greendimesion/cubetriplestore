package region;

import plane.PlaneCoordinate;
import query.Dimension;

public class RegionCoordinate implements Comparable<RegionCoordinate> {

    private static final int X = 0;
    private static final int Y = 1;
    private final short[] data;

    public RegionCoordinate() {
        this.data = new short[2];
    }

    public RegionCoordinate(PlaneCoordinate coordinate, int level) {
        this();
        this.data[X] = (short) (coordinate.getX() >> (16 + (level << 4)));
        this.data[Y] = (short) (coordinate.getY() >> (16 + (level << 4)));
    }

    public short getX() {
        return data[X];
    }

    public short getY() {
        return data[Y];
    }

    public short get(Dimension dimension) {
        return data[dimension.getId()];
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return equals((RegionCoordinate) obj);
    }

    public boolean equals(RegionCoordinate coordinate) {
        return (coordinate.data[X] == data[X]) && (coordinate.data[Y] == data[Y]);
    }

    @Override
    public int compareTo(RegionCoordinate o) {
        if (equals(o)) {
            return 0;
        }
        if (o.getX() < this.getX()) {
            return -1;
        }
        if (o.getX() == this.getX()) {
            if (o.getY() < this.getY()) {
                return -1;
            }
            return 1;
        }
        return 1;
    }
}
