package region;

import avltree.AvlNode;
import avltree.AvlTree;
import query.Dimension;
import triple.ItemList;

public class Region {    
    public final AvlTree<RegionCoordinate,Integer> cellList;
    
    public Region() {
        this.cellList = new AvlTree<RegionCoordinate, Integer>();
    }

    public int get(RegionCoordinate coordinate) {
        RegionCell cell = getCell(coordinate);
        return (cell != null) ? cell.value : -1; 
    }
    
    public void set(RegionCoordinate coordinate, int value) {
        checkOverride(coordinate);
        cellList.insert(coordinate, value);
    }
    
    public ItemList<Integer> search(Dimension dimension, short value) {
        ItemList<Integer> result = new ItemList<>();
        AvlNode<RegionCoordinate, Integer> root = cellList.getRoot();
        return result;
    }

    public Content getContent() {
        Content content = new Content(cellList.size());
//        int index = 0;
//        for (int i= 0; i<cellList.size(); i++)
//            content.set(index++, cellList.get(i));
        return content;
    }

    private RegionCell getCell(RegionCoordinate coordinate) {
//        for (int i= 0; i<cellList.size(); i++)
//            if (cellList.get(i).match(coordinate)) return cellList.get(i);
//        return null;
        Integer value = cellList.getValue(coordinate);
//        RegionCell value = cellList.getValue(new RegionCell(coordinate, 0));
        if (value != null) return new RegionCell(coordinate,value);
        return null;
        
    }
    
    private void checkOverride(RegionCoordinate coordinate) throws RuntimeException {
        if (getCell(coordinate) != null) 
            throw new RuntimeException("Impossible to overwrite the value");
    }

    private class RegionCell implements Comparable<RegionCell>{
        private RegionCoordinate coordinate;
        private int value;

        public RegionCell(RegionCoordinate coordinate, int value) {
            this.coordinate = coordinate;
            this.value = value;
        }
        
        public boolean match(RegionCoordinate coordinate) {
            return coordinate.equals(this.coordinate);
        }

        @Override
        public int compareTo(RegionCell o) {
            if (match(o.coordinate)) return 0;
            if (o.coordinate.getX() < this.coordinate.getX()) return -1;
            if (o.coordinate.getX() == this.coordinate.getX()){
                if (o.coordinate.getY() < this.coordinate.getY()) return -1;
                return 1;
            }
            return 1;
        }
    }

    public class Content {
        private final int[][] items;
        private static final int SIZE = 3;

        private Content(int count) {
            this.items = new int[count][SIZE];
        }
        
        private void set(int index, RegionCell cell) {
            items[index][0] = cell.coordinate.getX();
            items[index][1] = cell.coordinate.getY();
            items[index][2] = cell.value;
        }

        public int[][] getItems() {
            return items;
        }
        
    }
    
}
