package plane;

import java.io.Serializable;
import query.Dimension;
import triple.Triple;

public class PlaneCoordinate implements Serializable {
   
    private static final int X = 0;
    private static final int Y = 1;
    private final long x;
    private final long y;
    
    public PlaneCoordinate(long x, long y) {
        this.x = (x<<16)>>16;
        this.y = (y<<16)>>16;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    public long get(Dimension dimension) {
        long[] data = {x,y};
        return data[dimension.getId()];
    }

    public static PlaneCoordinate fromTriple(Triple triple) {
        return new PlaneCoordinate(triple.getSubject(), triple.getObject());
    }
    
}
