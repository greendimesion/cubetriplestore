package plane;

import java.io.DataOutputStream;
import page.Page;
import page.PageSerializer;
import java.io.IOException;
import triple.ItemList;

public class PlaneSerializer {

    private final PageSerializer pageSerialize;

    public PlaneSerializer() {
        this.pageSerialize = new PageSerializer();
    }

    public void serialize(DataOutputStream dataOutputStream, Plane plane) throws IOException {
        dataOutputStream.writeInt(plane.pageListsize());
        ItemList<Page> pageList = plane.getPageList();
        for (Page page : pageList) {
            pageSerialize.serialize(dataOutputStream, page);
        }
    }

}
