package plane;

import java.util.ArrayList;
import page.Page;
import page.PageCoordinate;
import query.Dimension;
import query.PageSearcher;
import query.SubregionSearcher;
import region.Region;
import region.RegionCoordinate;
import triple.ItemList;

public class Plane {
    public final Region region;
    public final ItemList<Region> subregions;
    private final ItemList<Page> pageList;

    public Plane() {
        this.region = new Region();
        this.subregions = new ItemList<>();
        this.pageList = new ItemList<>();
    }

    public ItemList<Page> getPageList() {
        return pageList;
    }

    public int pageListsize() {
        return pageList.size();
    }

    public Page get(PlaneCoordinate coordinate) {
        int index = getIndex(coordinate);
        if (index < 0) {
            return null;
        }
        return pageList.get(index);
    }

    public Page load(PlaneCoordinate coordinate) {
        Page page = get(coordinate);
        if (page != null) {
            return page;
        }
        return createPage(coordinate);
    }

    public Region.Content getContent() {
        return region.getContent();
    }

    public Region.Content getContent(int subregion) {
        return subregions.get(subregion).getContent();
    }

    public Page.Content getPageContent(int index) {
        Page page = pageList.get(index);
        return page.getContent();
    }

    private int getIndex(PlaneCoordinate coordinate) {
        Region subregion = getSubregion(new RegionCoordinate(coordinate, 1));
        if (subregion == null) {
            return -1;
        }
        return subregion.get(new RegionCoordinate(coordinate, 0));
    }

    private void setIndex(PlaneCoordinate coordinate, int index) {
        Region subregion = loadSubRegion(new RegionCoordinate(coordinate, 1));
        subregion.set(new RegionCoordinate(coordinate, 0), index);
    }

    private Region loadSubRegion(RegionCoordinate coordinate) {
        Region subregion = getSubregion(coordinate);
        if (subregion == null) {
            subregion = createSubregion(coordinate);
        }
        return subregion;
    }

    private Region getSubregion(RegionCoordinate coordinate) {
        int index = this.region.get(coordinate);
        if (index < 0) {
            return null;
        }
        return subregions.get(index);
    }

    private Region createSubregion(RegionCoordinate coordinate) {
        Region subregion = new Region();
        region.set(coordinate, subregions.size());
        subregions.add(subregion);
        return subregion;
    }

    private Page createPage(PlaneCoordinate coordinate) {
        Page page = new Page(coordinate);
        setIndex(coordinate, pageList.size()); 
        pageList.add(page);
        return page;
    }

//    public boolean search(PlaneCoordinate coordinate) {
//        Region subregion = getSubregion(new RegionCoordinate(coordinate, 1));
//        Page page = getPage(subregion, new RegionCoordinate(coordinate, 0));
//        return page.search(PageCoordinate.fromPlaneCoordinate(coordinate));
//    }

    public ArrayList<Long> search(final Dimension dimension, final long value) {
        ItemList<Integer> subregionIndex = region.search(dimension, (short) (value >> 24));
        ItemList<Integer> pageIndex;
        SubregionSearcher subregionSearcher = new SubregionSearcher(subregions, subregionIndex);
        pageIndex = subregionSearcher.execute(dimension, (short) (value >> 8));
        PageSearcher pageSearcher = new PageSearcher(pageList, pageIndex);
        return pageSearcher.execute(dimension, (byte) value);
    }
    
    private Page getPage(Region subregion, RegionCoordinate regionCoordinate) {
        return pageList.get(subregion.get(regionCoordinate));
    }
}
