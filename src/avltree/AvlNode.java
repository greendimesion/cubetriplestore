package avltree;

import java.io.Serializable;

public class AvlNode<T extends Comparable, R> implements Comparable<AvlNode>, Serializable{

    private T value;
    private R id;
    private AvlNode left;
    private AvlNode right;
    private int height = 0;

    public AvlNode(T value, R id) {
        this(value, null, null, id);
    }

    public R getId() {
        return id;
    }

    public AvlNode(T value, AvlNode leftNode, AvlNode rightNode, R id) {
        this.value = value;
        this.left = leftNode;
        this.right = rightNode;
        this.height = 0;
        this.id = id;
    }

    public AvlNode getLeft() {
        return left;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public AvlNode getRight() {
        return right;
    }

    public void setLeft(AvlNode left) {
        this.left = left;
    }

    public void setRight(AvlNode right) {
        this.right = right;
    }

    public boolean hasChilds() {
        return (left != null || right != null);
    }

    public T getValue() {
        return value;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public int compareTo(AvlNode o) {
        return this.getValue().compareTo(o.getValue());
    }
    
}