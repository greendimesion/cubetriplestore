package triplestore;

import java.io.DataInputStream;
import java.io.IOException;

public abstract class TripleStoreDeserializer {
    
    private final TripleStore tripleStore;

    public TripleStoreDeserializer(TripleStore tripleStore) {
        this.tripleStore = tripleStore;
    }

    public TripleStore getTripleStore() {
        return tripleStore;
    }
    
    public abstract void execute(DataInputStream stream) throws IOException;
        
}
