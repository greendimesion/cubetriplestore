package triplestore;

import java.io.DataOutputStream;
import java.io.IOException;

public abstract class TripleStoreSerializer {
    
    private final TripleStore tripleStore;

    public TripleStoreSerializer(TripleStore tripleStore) {
        this.tripleStore = tripleStore;
    }

    public TripleStore getTripleStore() {
        return tripleStore;
    }
    
    public abstract void execute(DataOutputStream stream) throws IOException;
    
}
