package triplestore;

import patternmatching.Pattern;
import patternmatching.PatternQuery;

public abstract class TripleStoreQuery {
    
    private final TripleStore tripleStore;
    
    public TripleStoreQuery(TripleStore tripleStore) {
        this.tripleStore = tripleStore;
    }

    public TripleStore getTripleStore() {
        return tripleStore;
    }
    
    public abstract PatternQuery createPatternQuery(Pattern pattern);

    
}
