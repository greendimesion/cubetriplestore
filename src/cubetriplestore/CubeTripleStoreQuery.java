package cubetriplestore;

import query.Dimension;
import java.util.HashMap;
import java.util.Map;
import patternmatching.ConstantSet;
import patternmatching.Pattern;
import patternmatching.PatternQuery;
import plane.Plane;
import triple.Triple;
import triplestore.TripleStoreQuery;

public class CubeTripleStoreQuery extends TripleStoreQuery {

    public CubeTripleStoreQuery(CubeTripleStore tripleStore) {
        super(tripleStore);
    }

    @Override
    public PatternQuery createPatternQuery(Pattern pattern) {
        return new PatternQuery(pattern) {
            @Override
            protected boolean checkPattern() {
                CubeTripleStore store = (CubeTripleStore) getTripleStore();
                return store.search(new Triple(getPatternSubject(),(int) getPatternPredicate(), getPatternObject()));
            }

            @Override
            protected ConstantSet getSubjects() {
                CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
                int predicate = (int) getPatternPredicate();
                ConstantSet result = new ConstantSet();
                if (cubeTripleStore.getPlane(predicate) != null) {
                    result.add(cubeTripleStore.getPlane(predicate).search(Dimension.Y, getPatternObject()));
                }
                return result;
            }

            @Override
            protected ConstantSet getPredicates() {
                CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
//                HashMap<Integer, Plane> planes = cubeTripleStore.getPlanes();
//                ConstantSet result = new ConstantSet();
//                for (Map.Entry<Integer, Plane> entry : planes.entrySet()) {
//                    Integer idPlane = entry.getKey();
//                    if (cubeTripleStore.checkTriple(new Triple(getPatternSubject(), idPlane, getPatternObject()))) {
//                        result.add((long) idPlane);
//                    }
//                }
//                return result;
                return null;
            }

            @Override
            protected ConstantSet getObjects() {
                CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
                int predicate = (int) getPatternPredicate();
                ConstantSet result = new ConstantSet();
                if (cubeTripleStore.getPlane(predicate) != null) {
                    result.add(cubeTripleStore.getPlane(predicate).search(Dimension.X, getPatternSubject()));
                }
                return result;
            }
        };
    }
}
