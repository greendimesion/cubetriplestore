package cubetriplestore;

import plane.PlaneCoordinate;
import plane.Plane;
import page.PageCoordinate;
import page.Page;
import java.util.HashMap;
import triple.ItemList;
import triple.Triple;
import triplestore.TripleStore;

public class CubeTripleStore extends TripleStore {

    private final ItemList<Plane> planes;

    public CubeTripleStore() {
        this.planes = new ItemList<>();
    }

    @Override
    public void assertTriple(Triple triple){
        Page page = loadPage(triple);
        page.add(PageCoordinate.fromTriple(triple));
    }

    @Override
    public void retractTriple(Triple triple) {
        Page page = loadPage(triple);
        page.remove(PageCoordinate.fromTriple(triple));
    }
    
    @Override
    public void clear() {
//        this.planes.clear();
    }

    @Override
    public boolean checkTriple(Triple triple) {
        Page page = getPage(triple);
        if (page == null) return false;
        return page.search(PageCoordinate.fromTriple(triple));
    }
    
    public ItemList<Plane> getPlanes() {
        return planes;
    }
    
    public Plane getPlane(Triple triple) {
        return planes.get(triple.getPredicate());
    }
    
    public Plane getPlane(int predicate) {
        return planes.get(predicate);
    }
    
    private Page getPage(Triple triple) {
        Plane plane = getPlane(triple);
        if (plane == null) return null;
        return plane.get(PlaneCoordinate.fromTriple(triple));
    }
    
    private Page loadPage(Triple triple) {
        Plane plane = loadPlane(triple);
        Page page = plane.load(PlaneCoordinate.fromTriple(triple));
        return page;
    }

    private Plane loadPlane(Triple triple) {
        Plane plane = planes.get(triple.getPredicate());
        if (plane == null) {
            plane = new Plane();
            planes.add(plane);
        }
        return plane;
    }

    @Override
    public boolean search(Triple triple) {
        Page page = loadPage(triple);
        if (page == null) return false;
        return page.search(PageCoordinate.fromTriple(triple));
    }
}
