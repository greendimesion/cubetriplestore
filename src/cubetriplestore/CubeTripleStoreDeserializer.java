package cubetriplestore;

import java.io.DataInputStream;
import java.io.IOException;
import triple.Triple;
import triplestore.TripleStore;
import triplestore.TripleStoreDeserializer;

public class CubeTripleStoreDeserializer extends TripleStoreDeserializer {


    public CubeTripleStoreDeserializer(TripleStore tripleStore) {
        super(tripleStore);
    }

    @Override
    public void execute(DataInputStream dataInputStream) throws IOException{
    }
    
    public void deserialize (DataInputStream dataInputStream) throws IOException {
        getTripleStore().clear();
        Triple triple = new Triple();
        CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
        int planesQuantity = dataInputStream.readInt();
        int predicate;
        int pagesQuantity;
        long coordinateX;
        long coordinateY;
        int itemsQuantity;
        
        for (int i = 0; i < planesQuantity; i++) {
            predicate = dataInputStream.readInt();
            System.out.println(predicate);
            pagesQuantity = dataInputStream.readInt();
            
            for(int j = 0; j < pagesQuantity; j++) {
                System.out.println("page number "+j);
                coordinateX = ((long)dataInputStream.readInt())<<8;
                coordinateY = ((long)dataInputStream.readInt())<<8;
                itemsQuantity = dataInputStream.readInt();
                
                for(int k = 0; k < itemsQuantity; k++){
                    cubeTripleStore.assertTriple(triple.buildTriple((long)(coordinateX + dataInputStream.readUnsignedByte()), predicate, (long)(coordinateY + dataInputStream.readUnsignedByte())));
                }
            }
        }
    }
    
}
