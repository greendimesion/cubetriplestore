package cubetriplestore;

import java.io.DataOutputStream;
import plane.PlaneSerializer;
import plane.Plane;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import page.Page;
import page.PageCoordinate;
import triple.ItemList;
import triplestore.TripleStore;
import triplestore.TripleStoreSerializer;

public class CubeTripleStoreSerializer extends TripleStoreSerializer {

    private final PlaneSerializer planeSerializer;

    public CubeTripleStoreSerializer(TripleStore tripleStore) {
        super(tripleStore);
        this.planeSerializer = new PlaneSerializer();
    }

    @Override
    public void execute(DataOutputStream dataOutputStream) throws IOException {
        ItemList<Plane> planes = ((CubeTripleStore) getTripleStore()).getPlanes();
        dataOutputStream.writeInt(planes.size());
        planeSerialize(planes, dataOutputStream);
    }

    private void planeSerialize(ItemList<Plane> planes, DataOutputStream dataOutputStream) throws IOException {
        int planeID = 0;
        for (Plane plane : planes) {
            dataOutputStream.writeInt(planeID++);
            pageSerialize(plane.getPageList(),dataOutputStream);
        }
    }

    private void pageSerialize(ItemList<Page> pageList, DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(pageList.size());
        for (Page page : pageList) {
            dataOutputStream.writeInt((int)(page.getCoordinate().getX() >>8));
            dataOutputStream.writeInt((int)(page.getCoordinate().getY() >>8));
            dataOutputStream.writeInt((int)page.size());
            short[] items = page.getContent().getData();
            for (int i = 0; i < (page.size()*2); i++) {
                dataOutputStream.writeShort(i);
            }

        }
    }

}
