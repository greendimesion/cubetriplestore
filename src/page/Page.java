package page;

import java.util.ArrayList;
import java.util.Iterator;
import plane.PlaneCoordinate;
import query.Dimension;

public class Page implements Iterable<PageCoordinate> {
    private static final int EXTENSION = 2;
    private static final int INITIALSIZE = 2;
    
    private final int x;
    private final int y;
    private short[] data;
    private int size;

    public Page(PlaneCoordinate planeCoordinate) {        
        this.x = pack(planeCoordinate.getX());
        this.y = pack(planeCoordinate.getY());
        this.data = allocData(INITIALSIZE);
        this.size = 0;
    }

    public PlaneCoordinate getCoordinate() {
        return new PlaneCoordinate(x << 8, y << 8);
    }
    
    public void add(PageCoordinate coordinate) {
        if (exists(coordinate)) return;
        if (getLength() == size) expand();
        setCoordinate(size, coordinate);
        this.size++;

    }

    public void remove(PageCoordinate coordinate) {
        byte index = 0;
        for (int i = 0; i < size; i++) {
            if (getCoordinate(i).equals(coordinate))
                continue;
            copyCoordinate(i, index);
            index++;
        }
        this.size = index;
    }

    public boolean exists(PageCoordinate coordinate) {
        for (int i = 0; i < size; i++)
            if (getCoordinate(i).equals(coordinate)) 
                return true;
        return false;
    }

    public Content getContent() {
        return new Content(data);
    }
    
    public int size() {
        return size;
    }

    private int count() {
        return this.data.length;
    }

    private PageCoordinate getCoordinate(int index) {
        index = index << 1;
        return new PageCoordinate(data[index], data[index+1]);
    }

    private void setCoordinate(int index, PageCoordinate coordinate) {
        index = index << 1;
        data[index + 0] = coordinate.getX();
        data[index + 1] = coordinate.getY();
    }

    private void copyCoordinate(int src, int dst) {
        if (src == dst) return;
        dst = dst << 1;
        src = src << 1;
        data[dst + 0] = data[src + 0];
        data[dst + 1] = data[src + 1];
    }

    private void copyCoordinates(short[] src, short[] dst) {
        System.arraycopy(src, 0, dst, 0, src.length);
    }

    public boolean search(PageCoordinate coordinate) {
        for (int i = 0; i<(size*2);i+=2){
            if (coordinate.getX() == data[i] && coordinate.getY() == data[i+1]) return true;
        }
        return false;
    }
    
    public ArrayList<Long> search(Dimension dimension, byte value) {
        ArrayList<Long> result = new ArrayList<>();
        for (int index = 0; index < (size * 2); index += 2)
            if (data[index + dimension.getId()] == value){
                result.add(getId(data[index + dimension.getOrthogonal().getId()], dimension.getOrthogonal()));
            }
        return result;
    }
    
    private long getId(short value, Dimension dimension) {
        if (dimension.getId() == 0) return (long) ((this.x << 8) + (value & 0xFF));
        return (long) ((this.y << 8) + (value & 0xFF));
    }

    private void expand() {
        short[] data = allocData(size + EXTENSION);
        copyCoordinates(this.data, data);
        this.data = data;
    }

    private int getLength() {
        return (data.length /2);
    }

    private short[] allocData(int length) {
        return new short[length << 1];
    }

    @Override
    public Iterator iterator() {
        return new Iterator<PageCoordinate>() {
            
            private int index;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public PageCoordinate next() {
                return getCoordinate(index++);
            }

            @Override
            public void remove() {
                
            }
        };
    }

    private int pack(long value) {
        return (int) (value >> 8);
    }

    public class Content {
        
        public static final byte X = 0;
        public static final byte Y = 1;
        private final short[] data;

        public Content(short[] data) {
            this.data = data;
        }

        public short[] getData() {
            return data;
        }
        
        public int size() {
            return size;
        }
        
    }
}