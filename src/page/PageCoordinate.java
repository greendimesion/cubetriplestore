package page;

import plane.PlaneCoordinate;
import query.Dimension;
import triple.Triple;

public class PageCoordinate {
    private final short x;
    private final short y;

    public PageCoordinate(short x, short y) {
        this.x = x;
        this.y = y;
    }
    
    public short getX() {
        return x;
    }
    
    public short getY() {
        return y;
    }
    
    public short get(Dimension dimension) {
        short[] data = {x,y};
        return data[dimension.getId()];
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        return equals((PageCoordinate) obj);
    }

    public boolean equals(PageCoordinate coordinate) {
        return (coordinate.x == x) &&  (coordinate.y == y);
    }
    
    public static final PageCoordinate fromTriple(Triple triple) {
        return new PageCoordinate((short) triple.getSubject(), (short) triple.getObject());
    }

//    public static final PageCoordinate fromPlaneCoordinate(PlaneCoordinate planeCoordinate) {
//        return new PageCoordinate((byte) planeCoordinate.getX(), (byte) planeCoordinate.getY());
//    }
    
}
