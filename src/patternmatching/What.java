package patternmatching;

public class What extends Operand {
    
    private final String name;

    public What(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}

