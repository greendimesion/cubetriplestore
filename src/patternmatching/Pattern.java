package patternmatching;

public class Pattern  {
    private final Operand subject;
    private final Operand predicate;
    private final Operand object;

    public Pattern(Operand subject, Operand predicate, Operand object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public Operand getSubject() {
        return subject;
    }

    public Operand getPredicate() {
        return predicate;
    }

    public Operand getObject() {
        return object;
    }
    
    public int getOrder() {
        return getOrder(subject) + getOrder(predicate) + getOrder(object);
    }
    
    private static int getOrder(Operand operand) {
        return (operand instanceof What) ? 1 : 0;
    }
     
    
}
