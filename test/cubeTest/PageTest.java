package cubeTest;

import org.junit.Test;
import page.Page;
import page.PageCoordinate;
import plane.PlaneCoordinate;
import triple.ItemList;

public class PageTest {

    public static final int PAGES = 117187;
    public int TUPLES = 256;

    public PageTest() {
    }

    @Test
    public void addTriples() {
        ItemList<Page> pages = new ItemList<>();
        for (int i = 0; i < PAGES; i++) {
            if (i == PAGES - 1) {
                TUPLES = 128;
            }
            pages.add(createPage());
        }
        showMemory();
        showPages(pages);
    }

    private Page createPage() {
        Page page = new Page(new PlaneCoordinate(0, 0));
        for (int i = 0; i < TUPLES; i++) {
            page.add(new PageCoordinate((byte) (i % 19), (byte) (i % 200)));
        }
        return page;
    }

    private static void showMemory() {
        System.out.println("Used memory:" + inKbytes(getUsedMemory()) + " Mb");
    }

    private static long getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();
        return runtime.totalMemory() - runtime.freeMemory();
    }

    private static double inKbytes(long memory) {
        return memory / (1024D * 1024D);
    }

    private int getRandomSize() {
        return 300 - (int) (Math.random() * 100);
    }

    private void showPages(ItemList<Page> pages) {
        System.out.println(pages.size());
        System.out.println("Ratio: " + (double) getUsedMemory() / (PAGES * TUPLES) + " bytes");
    }

}
