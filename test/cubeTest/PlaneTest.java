package cubeTest;

import org.junit.Test;
import plane.Plane;
import plane.PlaneCoordinate;

public class PlaneTest {
    public static final int TUPLES = 30000000;
    
    public PlaneTest() {
    }
    
    @Test
    public void addTriples() {
        Plane plane = new Plane();
        for (long i = 0; i < TUPLES; i++) 
            plane.load(createPlaneCoordinate(i));
        showMemory();
        showPlane(plane);
    }

    private static void showMemory() {
        System.out.println("Used memory:" + inKbytes(getUsedMemory()) + " Mb");
    }

    private static long getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();               
        return runtime.totalMemory() - runtime.freeMemory();
    }
    
    private static double inKbytes(long memory) {
        return memory / (1024D * 1024D);
    }

    private long getMemberX(long i) {
        return (long) i;// % 1000000;
    }
    
    private long getMemberY(long i) {
        return (long) 0;
    }

    private PlaneCoordinate createPlaneCoordinate(long i) {
        return new PlaneCoordinate(getMemberX(i)    , getMemberY(i));
    }

    private void showPlane(Plane plane) {
        System.out.println("Pages: " + plane.getPageList().size());
        for (int i = 0; i < plane.subregions.size(); i++) {
            System.out.println("Subregions Cells: " + plane.subregions.get(i).cellList.size());
            
        }
        
        System.out.println("RegionCells: " + plane.region.cellList.size());
        System.out.println("Ratio: " + (double) getUsedMemory() / TUPLES + " bytes");
    }
    
    
}
