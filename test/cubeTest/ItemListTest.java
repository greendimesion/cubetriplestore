package cubeTest;

import org.junit.Test;
import static org.junit.Assert.*;
import triple.ItemList;

public class ItemListTest {

    public ItemListTest() {
    }

    @Test
    public void addOneItem() {
        ItemList<Integer> list;
        list = new ItemList<>();
        list.add(23);
        assertEquals((Integer) 23, list.get(0));
    }

    @Test
    public void expandItemList() {
        ItemList<Integer> list;
        list = new ItemList<>();
        list.add(23);
        list.add(34);
        list.add(46);
        assertEquals((Integer) 46, list.get(2));
    }
    
    @Test
    public void removeItemList() {
        ItemList<Integer> list;
        list = new ItemList<>();
        list.add(23);
        list.add(34);
        list.add(46);
        list.remove(1);
        assertNotSame((Integer) 34, list.get(1));
        assertEquals((Integer) 46, list.get(1));
        
    }
    
}
