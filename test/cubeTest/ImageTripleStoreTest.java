package cubeTest;

import triple.Triple;
import cubetriplestore.CubeTripleStore;
import junit.framework.Assert;
import org.junit.Test;

public class ImageTripleStoreTest {

    public ImageTripleStoreTest() {
    }

    @Test
    public void addTriple() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 1));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1)));
        Assert.assertFalse(store.checkTriple(new Triple(0, 0, 0)));
        Assert.assertFalse(store.checkTriple(new Triple(0, 1, 0)));
        Assert.assertFalse(store.checkTriple(new Triple(1, 0, 0)));
    }

    @Test
    public void add2Triples() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 1));
        store.assertTriple(new Triple(0, 0, 2));
        Assert.assertFalse(store.checkTriple(new Triple(0, 0, 0)));
        Assert.assertFalse(store.checkTriple(new Triple(0, 1, 0)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 2)));
        Assert.assertFalse(store.checkTriple(new Triple(1, 0, 0)));
    }

    @Test
    public void addAndRemoveTriple() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 1));
        store.assertTriple(new Triple(0, 0, 3));
        store.retractTriple(new Triple(0, 0, 1));
        Assert.assertFalse(store.checkTriple(new Triple(0, 0, 1)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 3)));
    }

    @Test
    public void add12Triples() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 0));
        store.assertTriple(new Triple(0, 0, 1));
        store.assertTriple(new Triple(0, 1, 2));
        store.assertTriple(new Triple(0, 2, 3));
        store.assertTriple(new Triple(0, 2, 4));
        store.assertTriple(new Triple(0, 2, 5));
        store.assertTriple(new Triple(1, 0, 1));
        store.assertTriple(new Triple(1, 0, 0));
        store.assertTriple(new Triple(1, 1, 2));
        store.assertTriple(new Triple(1, 2, 3));
        store.assertTriple(new Triple(1, 2, 4));
        store.assertTriple(new Triple(1, 2, 6));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 0)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 1, 2)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 2, 3)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 2, 4)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 2, 5)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 0, 1)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 0, 0)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 1, 2)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 2, 3)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 2, 4)));
        Assert.assertTrue(store.checkTriple(new Triple(1, 2, 6)));
        Assert.assertFalse(store.checkTriple(new Triple(1, 0, 6)));
        Assert.assertFalse(store.checkTriple(new Triple(0, 4, 2)));
        Assert.assertFalse(store.checkTriple(new Triple(1, 3, 8)));
    }

    @Test
    public void addTripleGreaterThan256() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 256));
        store.assertTriple(new Triple(1 << 20, 1, 1 << 22));
        Assert.assertFalse(store.checkTriple(new Triple(0, 0, 0)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 256)));
        Assert.assertTrue(store.checkTriple(new Triple(1 << 20, 1, 1 << 22)));
        Assert.assertFalse(store.checkTriple(new Triple(1 << 20, 1 << 8, (1 << 22) - 1)));
        Assert.assertFalse(store.checkTriple(new Triple(1 << 20, 1 << 8, (1 << 22) + 1)));
        Assert.assertFalse(store.checkTriple(new Triple(1 << 4, 1 << 2, 1 << 10)));
    }

    @Test
    public void addTripleGreaterThan4Mega() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, (1 << 16)));
        store.assertTriple(new Triple(0, 0, (1 << 24)));
        store.assertTriple(new Triple(0, 0, (1 << 32)));
        Assert.assertFalse(store.checkTriple(new Triple(0, 0, 0)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1 << 16)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1<<24)));
        Assert.assertTrue(store.checkTriple(new Triple(0, 0, 1<<32)));
    }
 
}