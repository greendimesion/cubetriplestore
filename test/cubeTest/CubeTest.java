package cubeTest;

import cubetriplestore.CubeTripleStore;
import org.junit.Test;
import triple.Triple;

public class CubeTest {

    public static final int TRIPLES = 1000000;
    public static final int PREDICATES = 28;

    public CubeTest() {
    }

    @Test
    public void addTriples() {
        CubeTripleStore cubeTripleStore = new CubeTripleStore();
        for (int k = 0; k < PREDICATES; k++) {
            for (long i = 0; i < TRIPLES; i++) {
                cubeTripleStore.assertTriple(crateTriple(i,k));
            }
        }

        showMemory();
        showCube(cubeTripleStore);
    }

    private static void showMemory() {
        System.out.println("Used memory:" + inKbytes(getUsedMemory()) + " Mb");
    }

    private static long getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();
        return runtime.totalMemory() - runtime.freeMemory();
    }

    private static double inKbytes(long memory) {
        return memory / (1024D * 1024D);
    }

    private Triple crateTriple(long i, int k) {
        return new Triple(i, k, i);
    }

    private void showCube(CubeTripleStore cubeTripleStore) {

        for (int i = 0; i < cubeTripleStore.getPlanes().size(); i++) {
            System.out.println("");
            System.out.println("Predicado: " + i);
            System.out.println("Numero de subregiones: " + cubeTripleStore.getPlane(i).subregions.size());
            for (int j = 0; j < cubeTripleStore.getPlane(i).subregions.size(); j++) {
                System.out.println("Subregions Cells: " + cubeTripleStore.getPlane(i).subregions.get(j).cellList.size());
            }
            System.out.println("Numero de paginas: " + cubeTripleStore.getPlane(i).getPageList().size());
//            for (int k = 0; k < cubeTripleStore.getPlane(i).getPageList().size(); k++) {
//                System.out.print("Pagina: " + k);
//                System.out.println("Numero de duplas: " + cubeTripleStore.getPlane(i).getPageList().get(k).size());
//            }
        }

        System.out.println("Ratio: " + (double) getUsedMemory() / (TRIPLES*PREDICATES) + " bytes");
    }

}
