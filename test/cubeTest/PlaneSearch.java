package cubeTest;

import cubetriplestore.CubeTripleStore;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;
import triple.Triple;

public class PlaneSearch {

    @Test
    public void searchTest() {
        CubeTripleStore cubeTripleStore = new CubeTripleStore();
        cubeTripleStore.assertTriple(new Triple(0, 1, 1));
        cubeTripleStore.assertTriple(new Triple(0, 0, 0));
        cubeTripleStore.assertTriple(new Triple(0, 0, 1));
        cubeTripleStore.assertTriple(new Triple(0, 1, 2));
        cubeTripleStore.assertTriple(new Triple(0, 2, 3));
        cubeTripleStore.assertTriple(new Triple(0, 2, 4));
        cubeTripleStore.assertTriple(new Triple(0, 2, 5));
        cubeTripleStore.assertTriple(new Triple(1, 0, 1));
        cubeTripleStore.assertTriple(new Triple(1, 0, 0));
        cubeTripleStore.assertTriple(new Triple(1, 1, 2));
        cubeTripleStore.assertTriple(new Triple(1, 2, 3));
        cubeTripleStore.assertTriple(new Triple(1, 2, 4));
        cubeTripleStore.assertTriple(new Triple(1, 2, 6));
        assertTrue(cubeTripleStore.search(new Triple(0, 2, 4)));
    }

}
