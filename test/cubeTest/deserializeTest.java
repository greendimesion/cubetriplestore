package cubeTest;

import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreDeserializer;
import cubetriplestore.CubeTripleStoreSerializer;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;
import triple.Triple;

public class deserializeTest {

    public deserializeTest() {
    }

    @Test
    public void deserialize() throws IOException {

        File file = new File("serialized.dat");
        if (!file.exists()) {
            file.createNewFile();
        }
        CubeTripleStore cubeTripleStore = new CubeTripleStore();
        CubeTripleStoreDeserializer cubeTripleStoreDeserializer = new CubeTripleStoreDeserializer(cubeTripleStore);
        CubeTripleStoreSerializer cubeTripleStoreSerializer = new CubeTripleStoreSerializer(cubeTripleStore);
        DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
        cubeTripleStore.assertTriple(new Triple(0, 0, 1));
        cubeTripleStore.assertTriple(new Triple(0, 0, 0));
        cubeTripleStore.assertTriple(new Triple(0, 0, 1));
        cubeTripleStore.assertTriple(new Triple(0, 1, 2));
        cubeTripleStore.assertTriple(new Triple(0, 2, 3));
        cubeTripleStore.assertTriple(new Triple(0, 2, 4));
        cubeTripleStore.assertTriple(new Triple(0, 2, 5));
        cubeTripleStore.assertTriple(new Triple(1, 0, 1));
        cubeTripleStore.assertTriple(new Triple(1, 0, 0));
        cubeTripleStore.assertTriple(new Triple(1, 1, 2));
        cubeTripleStore.assertTriple(new Triple(1, 2, 3));
        cubeTripleStore.assertTriple(new Triple(1, 2, 4));
        cubeTripleStore.assertTriple(new Triple(1, 2, 6));
        cubeTripleStoreSerializer.execute(dataOutputStream);
        
        cubeTripleStoreDeserializer.execute(dataInputStream);
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 0, 1)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 0, 0)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 0, 1)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 1, 2)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 2, 3)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 2, 4)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(0, 2, 5)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 0, 1)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 0, 0)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 1, 2)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 2, 3)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 2, 4)));
        assertTrue(cubeTripleStore.checkTriple(new Triple(1, 2, 6)));
        assertFalse(cubeTripleStore.checkTriple(new Triple(1, 0, 6)));
        assertFalse(cubeTripleStore.checkTriple(new Triple(0, 4, 2)));
        assertFalse(cubeTripleStore.checkTriple(new Triple(1, 3, 8)));
    }
}