package patterns;

import junit.framework.Assert;
import org.junit.Test;
import patternmatching.Constant;
import patternmatching.Pattern;
import patternmatching.PatternQuery;
import cubetriplestore.CubeTripleStoreQuery;

public class SimplePatterns extends PatternInitiator{
    
    @Test
    public void simplePatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new Constant(3), new Constant(0), new Constant(6)));
        PatternQuery.Listener listener = new PatternQuery.Listener() {
            @Override
            public void finished(PatternQuery patternQuery) {
            }
        };
        patternQuery.addListener(listener);
        patternQuery.start();
        patternQuery.join();
        Assert.assertTrue(patternQuery.getResult());
    }
    
   @Test
    public void falseSimplePatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new Constant(3), new Constant(5), new Constant(6)));
        PatternQuery.Listener listener = new PatternQuery.Listener() {
            @Override
            public void finished(PatternQuery patternQuery) {
            }
        };
        patternQuery.addListener(listener);
        patternQuery.start();
        patternQuery.join();
        Assert.assertFalse(patternQuery.getResult());
    }
    
    
}
