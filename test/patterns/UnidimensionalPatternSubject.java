package patterns;

import junit.framework.Assert;
import org.junit.Test;
import patternmatching.Constant;
import patternmatching.Pattern;
import patternmatching.PatternQuery;
import patternmatching.What;
import cubetriplestore.CubeTripleStoreQuery;

public class UnidimensionalPatternSubject extends PatternInitiator{
    
    @Test
    public void oneDimensionSubjectPatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new What("a"), new Constant(0), new Constant(6)));
        patternQuery.start();
        patternQuery.join();
        Assert.assertTrue(patternQuery.getResult());
        Assert.assertEquals(1, patternQuery.getSet("a").size());
        Assert.assertEquals(3, patternQuery.getSet("a").get(0));
    }
    
    @Test
    public void falseOneDimensionSubjectPatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new What("a"), new Constant(5), new Constant(6)));
        PatternQuery.Listener listener = new PatternQuery.Listener() {
            @Override
            public void finished(PatternQuery patternQuery) {
            }
        };
        patternQuery.addListener(listener);
        patternQuery.start();
        patternQuery.join();
        Assert.assertFalse(patternQuery.getResult());
        Assert.assertEquals(0, patternQuery.getSet("a").size());
    }
    
}
