package patterns;

import junit.framework.Assert;
import org.junit.Test;
import patternmatching.Constant;
import patternmatching.Pattern;
import patternmatching.PatternQuery;
import patternmatching.What;
import cubetriplestore.CubeTripleStoreQuery;

public class UnidimensionalPatternPredicate extends PatternInitiator{
    
    @Test
    public void OneDimensionPredicatePatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new Constant(3), new What("a"), new Constant(6)));
        PatternQuery.Listener listener = new PatternQuery.Listener() {
            @Override
            public void finished(PatternQuery patternQuery) {
            }
        };
        patternQuery.addListener(listener);
        patternQuery.start();
        patternQuery.join();
        Assert.assertTrue(patternQuery.getResult());
        Assert.assertEquals(1, patternQuery.getSet("a").size());
        Assert.assertEquals(4, patternQuery.getSet("a").get(0));
    }
    
    @Test
    public void falseOneDimensionPredicatePatternCheck() throws InterruptedException {
        CubeTripleStoreQuery queryInterface = new CubeTripleStoreQuery(initStore());
        PatternQuery patternQuery = queryInterface.createPatternQuery(
                new Pattern(new Constant(4), new What("a"), new Constant(6)));
        PatternQuery.Listener listener = new PatternQuery.Listener() {
            @Override
            public void finished(PatternQuery patternQuery) {
            }
        };
        patternQuery.addListener(listener);
        patternQuery.start();
        patternQuery.join();
        Assert.assertFalse(patternQuery.getResult());
        Assert.assertEquals(0, patternQuery.getSet("a").size());
    }
    
}
