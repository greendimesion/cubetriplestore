package patterns;

import org.junit.Test;
import triple.Triple;
import cubetriplestore.CubeTripleStore;

public class PatternInitiator {
    
    private CubeTripleStore store;

    public CubeTripleStore initStore() {
        store = new CubeTripleStore();
        store.assertTriple(new Triple(3, 0, 6));
        return store;
    }
    
    @Test
    public void patternTest(){
        return;
    }
    
}
