package cubetriplestore;

import org.junit.Test;
import triple.Triple;


public class CubeTripleStoreSerializerTest {
    
    public CubeTripleStoreSerializerTest() {
    }
    
    @Test
    public void serialization(){
        CubeTripleStore cubeTripleStore = new CubeTripleStore();
        
        cubeTripleStore.assertTriple(new Triple(0, 0, 1));
    }
    
    
}
